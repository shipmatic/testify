from django.contrib.auth.forms import UserChangeForm, UserCreationForm


class AccountCreateForm(UserCreationForm):
    
    class Meta(UserCreationForm.Meta):
        fields = ['username', 'first_name', 'last_name', 'email']


class AccountUpdateForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        fields = ['username', 'first_name', 'last_name', 'email']
