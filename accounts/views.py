# import datetime
from accounts.forms import (AccountCreateForm, 
                            AccountUpdateForm)

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from django.urls import reverse_lazy
from django.urls.base import reverse
from django.views.generic import CreateView 
from django.views.generic.edit import UpdateView


class AccountCreateView(CreateView):
    model = User
    template_name = 'registration.html'
    form_class = AccountCreateForm
    success_url = reverse_lazy('accounts:login')  # это редирект после завершения регистрации

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, 'User successfully created')
        return result


class AccountLoginView(LoginView):
    model = User
    template_name = 'login.html'
    success_url = reverse_lazy('link')

    # функция ниже переправляет на страницу откужа пришел пользователь
    # Если сработал LoginRequiredMixin

    def get_redirect_url(self):
        if self.request.GET.get('next'):  # это параметр Get запроса проверяет, есть ли 
            return self.request.GET.get('next')  # next в адресной строке
        return reverse('core:index')

    def form_valid(self, form):

        result = super().form_valid(form)
        messages.info(self.request, f'User {self.request.user} logged in!')

        return result


class AccountLogoutView(LogoutView):
    template_name = 'logout.html'
    
    def get(self, request, *args, **kwargs):
        result = super().get(request, *args, **kwargs)
        messages.info(self.request, f'User {self.request.user} has been logged out!')
        return result


class AccountUpdateView(LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy('accounts:login')
    model = User
    template_name = 'profile.html'
    form_class = AccountUpdateForm
    success_url = reverse_lazy('link')  

    def get_object(self, queryset=None):
        return self.request.user


class AccountPasswordView(LoginRequiredMixin, PasswordChangeView):
    login_url = reverse_lazy('accounts:login')
    template_name = 'password.html'
    success_url = reverse_lazy('link')  
